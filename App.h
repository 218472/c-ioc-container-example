//
// Created by teodorek on 06.12.18.
//

#ifndef DI_APP_H
#define DI_APP_H
#include "IReader.h"

class App
{
public:
     explicit App(std::unique_ptr<const IReader> reader) : _reader(std::move(reader)){}
     void Read(std::ostream& out) const { out << _reader->Read();}

private:
  std::unique_ptr<const IReader> _reader;
};


#endif //DI_APP_H
