//
// Created by teodorek on 06.12.18.
//

#ifndef DI_IREADER_H
#define DI_IREADER_H


#include <string>

class IReader
{
public:
    virtual std::string Read() const = 0;
};


#endif //DI_IREADER_H
