//
// Created by teodorek on 06.12.18.
//

#pragma once

#include "IReader.h"

class IexApiReader : public IReader
{
public:
    virtual std::string Read() const override {return std::string("IexApiReader");}
};

