//
// Created by teodorek on 06.12.18.
//
#pragma once

#include <boost/di.hpp>
#include "../FileReader.h"
#include "../IexApiReader.h"

namespace di = boost::di;

class IReader;
class FileReader;
class IexApiReader;

const auto config = []
{
  return di::make_injector(di::bind<IReader>().to<IexApiReader>());
};