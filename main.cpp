#include <iostream>

#include "common/config.hpp"
#include "App.h"

int main()
{
  auto app = config().create<App>();
  app.Read(std::cout);
}